export const environment = {
	production: false,
	config: {
		autoFocusAssist: false,
		menuType: 'overlay'
	},
	// server_url: 'http://139.162.5.110:8082/',
	server_url: 'https://test.mindnerves.com/api/',
	// Set language to use.
	language: 'en',
	// Loading Configuration.
	loading: {
		spinner: 'bubbles'
	},
	// Toast Configuration.
	toast: {
		position: 'bottom' // Position of Toast, top, middle, or bottom.
	},
	toastDuration: 3000, // Duration (in milliseconds) of how long toast messages should show before they are hidden.
};
