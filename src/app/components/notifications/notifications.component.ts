import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';

@Component({
	selector: 'app-notifications',
	templateUrl: './notifications.component.html',
	styleUrls: ['./notifications.component.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
				query(':enter', stagger('200ms', [animate('400ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class NotificationsComponent {
	messages: Array<any> = [];

	constructor(public popoverController: PopoverController) {

	}

	close() {
		this.popoverController.dismiss();
	}


}
