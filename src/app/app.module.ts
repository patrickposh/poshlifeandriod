import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateProvider } from './providers';
import { Geolocation } from '@ionic-native/geolocation/ngx'
import { ImagePageModule } from './pages/modal/image/image.module';
import { SearchFilterPageModule } from './pages/modal/search-filter/search-filter.module';
import { SchoolsInfoPageModule } from './pages/modal/schools-info/schools-info.module';
import { NearbySoldRentModule } from './pages/modal/nearby-sold-rent/nearby-sold-rent.module';
import { BlogInfoPageModule } from './pages/modal/blog-info/blog-info.module';
import { PropertyTypePageModule } from './pages/modal/property-type/property-type.module';
import { environment } from '../environments/environment';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { PipesModule } from './pipes/pipes.module';
import { ApiConfigService } from './api.service';
import { DataService } from './data.service';
import { EditProfileService } from './pages/edit-profile/edit-profile.service';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [AppComponent, NotificationsComponent],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		IonicModule.forRoot(environment.config),
		AppRoutingModule,
		HttpClientModule,
		HttpModule,
		ImagePageModule,
		SearchFilterPageModule,
		SchoolsInfoPageModule,
		NearbySoldRentModule,
		BlogInfoPageModule,
		PropertyTypePageModule,
		IonicStorageModule.forRoot({
			name: '__ionproperty2',
			driverOrder: ['indexeddb', 'sqlite', 'websql']
		}),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
		PipesModule,
		ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
	],
	entryComponents: [NotificationsComponent],
	providers: [
		StatusBar,
		SplashScreen,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		TranslateProvider,
		Geolocation,
		ApiConfigService,
		DataService,
		EditProfileService
	],
	bootstrap: [AppComponent]
})

export class AppModule { }
