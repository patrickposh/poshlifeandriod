import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ResetpwdPage } from './resetpwd.page';
import { ResetpwdService } from './resetpwd.service';

const routes: Routes = [
	{
		path: '',
		component: ResetpwdPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		IonicModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild()
	],
	declarations: [
		ResetpwdPage
	],
	providers: [
		ResetpwdService
	]
})

export class ResetpwdPageModule { }
