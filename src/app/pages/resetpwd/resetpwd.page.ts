import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { NavController, MenuController, ToastController, LoadingController } from '@ionic/angular';
import { TranslateProvider } from '../../providers';
import { takeUntil } from 'rxjs/operators';
import { ResetpwdService } from './resetpwd.service';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-resetpwd',
	templateUrl: './resetpwd.page.html',
	styleUrls: ['./resetpwd.page.scss'],
})

export class ResetpwdPage implements OnInit {

	resetForm: FormGroup;
	private _unsubscribeAll: Subject<any>;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

	constructor(public navCtrl: NavController, public menuCtrl: MenuController,
		public loadingCtrl: LoadingController, public toastCtrl: ToastController,
		private resetService: ResetpwdService, private formBuilder: FormBuilder) {

		this._unsubscribeAll = new Subject();
	}

	ngOnInit() {
		this.registerFormDetails();
	}

	registerFormDetails() {
		this.resetForm = this.formBuilder.group({
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			password: ['', Validators.required],
			confirmPassword: ['', [Validators.required, confirmPasswordValidator]]
		});

		this.resetForm.get('password').valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
			this.resetForm.get('confirmPassword').updateValueAndValidity();
		});
	}

	async resetPassword(value) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();
		this.resetService.resetPassword(value).subscribe(async data => {
			await loading.dismiss();
			if (data.code == 200) {
				this.presentToast('Password Reset Successfully');
				localStorage.removeItem('userDetails');
				localStorage.removeItem('modalShown');
				this.navCtrl.navigateRoot('/login');
				setTimeout(() => {
					window.location.reload();
				}, 300);
			} else {
				this.presentToast('Something went wrong');
			}
		}, err => {
			loading.dismiss();
			this.presentToast(err);
		})
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	login() {
		this.navCtrl.navigateRoot('/login');
	}
}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

	if (!control.parent || !control) {
		return null;
	}

	const password = control.parent.get('password');
	const confirmPassword = control.parent.get('confirmPassword');

	if (!password || !confirmPassword) {
		return null;
	}

	if (confirmPassword.value === '') {
		return null;
	}

	if (password.value === confirmPassword.value) {
		return null;
	}

	return { passwordsNotMatching: true };
};