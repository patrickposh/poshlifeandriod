import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.page.html',
	styleUrls: ['./notification.page.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
				query(':enter', stagger('200ms', [animate('400ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class NotificationPage implements OnInit {
	constructor() { }

	ngOnInit() { }
}
