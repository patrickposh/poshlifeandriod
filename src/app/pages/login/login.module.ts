import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './login.page';
import { LoginService } from './login.service';
import { Facebook } from '@ionic-native/facebook/ngx';

const routes: Routes = [
	{
		path: '',
		component: LoginPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatInputModule,
		IonicModule,
		TranslateModule.forChild(),
		RouterModule.forChild(routes),
	],
	declarations: [
		LoginPage
	],
	providers: [
		LoginService,
		Facebook
	]
})

export class LoginPageModule { }
