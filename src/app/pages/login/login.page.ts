import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NavController, MenuController, ToastController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
	public onLoginForm: FormGroup;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

	constructor(public navCtrl: NavController, public menuCtrl: MenuController,
		public toastCtrl: ToastController, private loginService: LoginService,
		public loadingCtrl: LoadingController, private formBuilder: FormBuilder,
		public router: Router, private facebook: Facebook) { }

	ionViewWillEnter() {
		this.menuCtrl.enable(false);
		this.menuCtrl.swipeEnable(false);
	}

	ngOnInit() {
		this.loginFormDetails();
	}

	loginFormDetails() {
		this.onLoginForm = this.formBuilder.group({
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			password: ''
		});
	}

	async login(inputData) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();
		let loginData = {};
		loginData['email'] = inputData.email;
		loginData['password'] = inputData.password;
		this.loginService.login(loginData).subscribe(async res => {
			if (res.code === 200 && res.data.token) {
				localStorage.setItem("userDetails", JSON.stringify(res));
				this.menuCtrl.enable(true);
				await loading.dismiss();
				setTimeout(() => {
					window.location.reload();
					// this.router.navigateByUrl('/tabs/(home:home)');
				}, 500);
			} else {
				await loading.dismiss();
				if (res.errorKey === "email400") {
					this.presentToast("This Email Id is Invalid");
				} else if (res.errorKey === "password400") {
					this.presentToast("This Password is Invalid");
				} else if (res.errorKey === "active400") {
					this.presentToast("Please Activate Your Account");
				}
			}
		}, err => {
			this.presentToast("Something Went Wrong");
			loading.dismiss();
		});
	}

	loginWithFacebok() {
		var userData: any = null;;
		this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
			this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
				userData = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'] }
				console.log("d",userData);
			});
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	signUpPage() {
		this.navCtrl.navigateRoot('/register');
	}

	forgotPassword() {
		this.navCtrl.navigateRoot('/forgotpwd-view');
	}
}
