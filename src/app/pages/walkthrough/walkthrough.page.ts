import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, IonSlides, MenuController } from '@ionic/angular';

@Component({
	selector: 'app-walkthrough',
	templateUrl: './walkthrough.page.html',
	styleUrls: ['./walkthrough.page.scss'],
})

export class WalkthroughPage {
	@ViewChild(IonSlides, { static: true }) slides: IonSlides;
	showSkip = true;
	slideOpts = {
		effect: 'flip',
		speed: 1000
	};
	dir: String = 'ltr';

	constructor( public navCtrl: NavController, public menuCtrl: MenuController,
		public router: Router ) {
		
		this.menuCtrl.enable(false);
	}

	openHomeLocation() {
		this.router.navigateByUrl('/tabs/(home:home)');
	}

	openLoginPage() {
		this.navCtrl.navigateForward('/login');
	}
}