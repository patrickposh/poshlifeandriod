import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { MatExpansionModule, MatSelectModule, MatListModule, MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { LeadGenerationPage } from './lead-generation.page';
import { EditProfileService } from '../edit-profile/edit-profile.service';
import { LeadGenerationService } from './lead-generation.service';

const routes: Routes = [
	{
		path: '',
		component: LeadGenerationPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		MatExpansionModule,
		MatSelectModule,
		MatIconModule,
		MatListModule,
		MatFormFieldModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild(),
	],
	declarations: [
		LeadGenerationPage
	],
	providers: [
		LeadGenerationService,
		EditProfileService
	]
})
export class LeadGenerationPageModule { }