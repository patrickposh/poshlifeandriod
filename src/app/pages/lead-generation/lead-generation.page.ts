import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { LeadGenerationService } from './lead-generation.service';
import { EditProfileService } from '../edit-profile/edit-profile.service';
import * as L from "leaflet";
import 'leaflet-draw';

@Component({
	selector: 'app-lead-generation',
	templateUrl: './lead-generation.page.html',
	styleUrls: ['./lead-generation.page.scss'],
})

export class LeadGenerationPage implements OnInit, AfterViewInit {

	leadGenerationForm: FormGroup;
	public basementArray = [];
	public propertyTypesArray = [];
	public minPriceList = [];
	public maxPriceList = [];
	public minSquareFootageList = [];
	public maxSquareFootageList = [];
	saleFlag: boolean = true;
	residentialFlag: boolean = true;
	saleOrLease: any = 'Sale';
	propertyType: any = 'residential';
	bedRooms: any[] = [];
	bathRoom: any = '';
	garageParking: any = '';
	bedroomAny: boolean = true;
	bedroom1: boolean = false;
	bedroom2: boolean = false;
	bedroom3: boolean = false;
	bedroom4: boolean = false;
	bedroom5: boolean = false;
	bathroomAny: boolean = true;
	bathroom1: boolean = false;
	bathroom2: boolean = false;
	bathroom3: boolean = false;
	bathroom4: boolean = false;
	bathroom5: boolean = false;
	gpAny: boolean = true;
	gp1: boolean = false;
	gp2: boolean = false;
	gp3: boolean = false;
	gp4: boolean = false;
	gp5: boolean = false;
	polyData: any = [];
	currentLat: any = "";
	currentLng: any = "";
	map: L.Map;
	marker: any;
	draw: L.Control.Draw;
	markerIcon: any = L.icon({
		iconUrl: '../../assets/icon/marker-icon.png',
		iconSize: [23, 40],
		iconAnchor: [9, 21],
		popupAnchor: [0, -14]
	});
	loginDetails: any;
	userId = null;
	name: any;
	email: any;
	phone: any;

	constructor(public router: Router, public loadingCtrl: LoadingController,
		public formBuilder: FormBuilder, public toastCtrl: ToastController,
		private geolocation: Geolocation, public profileService: EditProfileService,
		public leadGenerationService: LeadGenerationService, private data: DataService) {

		this.loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (this.loginDetails != undefined) {
			this.userId = JSON.stringify(this.loginDetails.data.userVM.id);
		}

		this.geolocation.getCurrentPosition().then((resp) => {
			this.currentLat = (resp.coords.latitude).toString();
			this.currentLng = (resp.coords.longitude).toString();
		}, err => {
			this.currentLat = "43.63943345";
			this.currentLng = "-79.39972759226308";
		});
	}

	ngOnInit() {
		this.leadGenerationFormDetails();
		this.getBasementTypes();
		this.getMinPrices();
		this.getMaxPrices();
		this.getMinSquareFootage();
		this.getMaxSquareFootage();
		this.getPropertyTypes('res');
	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.loadMap();
			this.getUserDetails(this.userId);
		}, 500);
	}

	leadGenerationFormDetails() {
		this.leadGenerationForm = this.formBuilder.group({
			propertyOptions: '',
			basementStyle: '',
			listPriceMin: null,
			listPriceMax: null,
			approxSquareFootageMin: null,
			approxSquareFootageMax: null,
			message: ''
		});
	}

	getUserDetails(userId) {
		if (userId != null) {
			this.profileService.getUserDetails(userId).subscribe(res => {
				if (res.code == 200) {
					this.name = res.data.firstname + ' ' + res.data.lastname;
					this.email = res.data.email;
					this.phone = res.data.phoneNumber;
				}
			});
		}
	}

	async subscribeToDailyNews(value) {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		await loading.present();
		var subscriptionData = {
			name: this.name,
			email: this.email,
			phone: this.phone,
			userId: this.userId,
			areaMin: value.approxSquareFootageMin,
			areaMax: value.approxSquareFootageMax,
			priceMin: value.listPriceMin,
			priceMax: value.listPriceMax,
			message: value.message,
			bedRooms: this.bedRooms,
			saleLease: this.saleOrLease,
			propertyType: this.propertyType,
			propertyOption: value.propertyOptions,
			washrooms: this.bathRoom,
			parkingSpaces: this.garageParking,
			basementStyle: value.basementStyle,
			isActive: true,
			polygonData: this.polyData
		}

		this.leadGenerationService.subscribeDailyNewProperties(subscriptionData).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.presentToast('Subscription Added Successfully');
				this.router.navigateByUrl('/home-results');
			} else {
				this.presentToast('Something went wrong');
			}
		}, err => {
			loading.dismiss();
			this.presentToast('Something went wrong');
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	saleLease(value) {
		if (value == 'lease') {
			this.saleFlag = false;
			this.saleOrLease = 'Lease';
		} else {
			this.saleFlag = true;
			this.saleOrLease = 'Sale';
		}
	}

	loadMap() {
		this.map = new L.Map('subscribeMap');
		this.map.setView([this.currentLat, this.currentLng], 12);
		this.map.setMinZoom(2);
		L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
			maxZoom: 18,
			id: 'mapbox/streets-v11',
			tileSize: 512,
			zoomOffset: -1,
			accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
		}).addTo(this.map);

		this.marker = L.marker([this.currentLat, this.currentLng], { icon: this.markerIcon }).addTo(this.map);

		var editableLayers = new L.FeatureGroup();
		this.map.addLayer(editableLayers);

		var drawPluginOptions: any = {
			position: 'topright',
			draw: {
				polygon: {
					allowIntersection: false, // Restricts shapes to simple polygons
					drawError: {
						color: '#e1e100', // Color the shape will turn when intersects
						message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
					},
					shapeOptions: {
						color: '#97009c'
					}
				},
				// disable toolbar item by setting it to false
				polyline: false,
				circle: false, // Turns off this drawing tool
				rectangle: false,
				marker: false,
			},
			edit: {
				featureGroup: editableLayers, //REQUIRED!!
				remove: true
			}
		};

		// Initialise the draw control and pass it the FeatureGroup of editable layers
		var drawControl = new L.Control.Draw(drawPluginOptions);
		this.map.addControl(drawControl);

		var editableLayers = new L.FeatureGroup();
		this.map.addLayer(editableLayers);

		this.map.on('draw:created', e => {
			var layer = e.layer;

			editableLayers.addLayer(layer);

			var cordinates = layer.toGeoJSON();
			let tempArr = [];
			(cordinates.geometry.coordinates[0]).forEach(element => {
				var obj = {
					latitude: element[1],
					longitude: element[0]
				}
				tempArr.push(obj);
			});
			this.getPolygonData(tempArr);
		});
	}

	getPolygonData(data) {
		this.polyData = data;
	}

	propertyTypeChange(value) {
		if (value == 'commercial') {
			this.getPropertyTypes('commer');
			this.residentialFlag = false;
			this.propertyType = 'commercial';
			this.bedRooms = [];
			this.bathRoom = "";
			this.garageParking = "";
		} else {
			this.getPropertyTypes('res');
			this.residentialFlag = true;
			this.propertyType = 'residential';
		}
	}

	bedroom(value) {
		this.bedroomAny = false;
		if (value === 'any') {
			this.bedroom1 = false;
			this.bedroom2 = false;
			this.bedroom3 = false;
			this.bedroom4 = false;
			this.bedroom5 = false;
			this.bedroomAny = true;
			this.bedRooms = [];
		} else if (value === '1') {
			if (this.bedroom1) {
				if (this.bedRooms.includes("1")) {
					const index = this.bedRooms.indexOf('1');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("1");
			}
			if (this.bedroom5 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom1) {
					if (!this.bedroom2 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom1 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom1 = false;
					}
				} else {
					this.bedroom1 = true;
				}
			}
		} else if (value === '2') {
			if (this.bedroom2) {
				if (this.bedRooms.includes("2")) {
					const index = this.bedRooms.indexOf('2');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("2");
			}
			if (this.bedroom1 && this.bedroom5 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom2) {
					if (!this.bedroom1 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom2 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom2 = false;
					}
				} else {
					this.bedroom2 = true;
				}
			}
		} else if (value === '3') {
			if (this.bedroom3) {
				if (this.bedRooms.includes("3")) {
					const index = this.bedRooms.indexOf('3');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("3");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom5 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom3) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom3 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom3 = false;
					}
				} else {
					this.bedroom3 = true;
				}
			}
		} else if (value === '4') {
			if (this.bedroom4) {
				if (this.bedRooms.includes("4")) {
					const index = this.bedRooms.indexOf('4');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("4");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom5) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom4) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom5) {
						this.bedroom4 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom4 = false;
					}
				} else {
					this.bedroom4 = true;
				}
			}
		} else if (value === '5') {
			if (this.bedroom5) {
				if (this.bedRooms.includes("5+")) {
					const index = this.bedRooms.indexOf('5+');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("5+");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom5) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom4) {
						this.bedroom5 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom5 = false;
					}
				} else {
					this.bedroom5 = true;
				}
			}
		}
	}

	bathroom(value) {
		this.bathroom1 = false;
		this.bathroom2 = false;
		this.bathroom3 = false;
		this.bathroom4 = false;
		this.bathroom5 = false;
		this.bathroomAny = false;
		if (value === 'any') {
			this.bathroomAny = true;
			this.bathRoom = '';
		} else if (value === '1') {
			this.bathroom1 = true;
			this.bathRoom = '1';
		} else if (value === '2') {
			this.bathroom2 = true;
			this.bathRoom = '2';
		} else if (value === '3') {
			this.bathroom3 = true;
			this.bathRoom = '3';
		} else if (value === '4') {
			this.bathroom4 = true;
			this.bathRoom = '4';
		} else if (value === '5') {
			this.bathroom5 = true;
			this.bathRoom = '5';
		}
	}

	gp(value) {
		this.gp1 = false;
		this.gp2 = false;
		this.gp3 = false;
		this.gp4 = false;
		this.gp5 = false;
		this.gpAny = false;
		if (value === 'any') {
			this.gpAny = true;
			this.garageParking = '';
		} else if (value === '1') {
			this.gp1 = true;
			this.garageParking = '1';
		} else if (value === '2') {
			this.gp2 = true;
			this.garageParking = '2';
		} else if (value === '3') {
			this.gp3 = true;
			this.garageParking = '3';
		} else if (value === '4') {
			this.gp4 = true;
			this.garageParking = '4';
		} else if (value === '5') {
			this.gp5 = true;
			this.garageParking = '5';
		}
	}

	getPropertyTypes(type) {
		this.propertyTypesArray = [];
		if (type == 'res') {
			this.propertyTypesArray = [
				{ id: 1, name: 'Condo Apt' },
				{ id: 2, name: 'Condo Townhouse' },
				{ id: 3, name: 'Detached' },
				{ id: 4, name: 'Semi-Detached' },
				{ id: 5, name: 'Vacant Land' }
			]
		} else {
			this.propertyTypesArray = [
				{ id: 1, name: 'Business' },
				{ id: 2, name: 'Office' },
				{ id: 3, name: 'All' }
			]
		}
	}

	getBasementTypes() {
		this.basementArray = this.data.getBasements();
	}

	getMinPrices() {
		this.minPriceList = this.data.getMinPrices();
	}

	getMaxPrices() {
		this.maxPriceList = this.data.getMaxPrices();
	}

	getMinSquareFootage() {
		this.minSquareFootageList = this.data.getMinSquareFootage();
	}

	getMaxSquareFootage() {
		this.maxSquareFootageList = this.data.getMaxSquareFootage();
	}
}