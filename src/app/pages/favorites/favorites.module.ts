import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { FavoritesPage } from './favorites.page';
import { FavoritesService } from './favorites.service';

const routes: Routes = [
	{
		path: '',
		component: FavoritesPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild()
	],
	declarations: [
		FavoritesPage
	],
	providers: [
		FavoritesService
	]
})

export class FavoritesPageModule { }
