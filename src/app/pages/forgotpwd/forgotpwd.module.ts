import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { IonicModule } from '@ionic/angular';
import { ForgotpwdPage } from './forgotpwd.page';
import { ForgotpwdService } from './forgotpwd.service';

const routes: Routes = [
	{
		path: '',
		component: ForgotpwdPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		IonicModule,
		TranslateModule.forChild(),
		RouterModule.forChild(routes)
	],
	declarations: [
		ForgotpwdPage
	],
	providers: [
		ForgotpwdService
	]
})

export class ForgotpwdPageModule { }
