import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiConfigService } from '../../api.service';
import { map } from "rxjs/operators";

@Injectable()
export class ForgotpwdService {
    
    options: any;

    constructor(public http: Http, public apiService: ApiConfigService) {

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new RequestOptions({ headers: headers });
    }

    forgotPassword(data) {
        return this.http.post(this.apiService.config + 'forgetPassword', data,
            this.options).pipe(map(response => response.json()));
    }
}