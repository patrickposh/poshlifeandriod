import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiConfigService } from '../../api.service';
import { map } from "rxjs/operators";

@Injectable()
export class PropertyDetailService {

    options: any;

    constructor(public http: Http, public apiService: ApiConfigService) {

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new RequestOptions({ headers: headers });
    }

    getPropertyDetails(propertyId) {
        return this.http.get(this.apiService.config + 'property/' + propertyId,
            this.options).pipe(map(response => response.json()));
    }

    addToFavorite(data) {
        return this.http.post(this.apiService.config + 'favorite/', data,
            this.options).pipe(map(response => response.json()));
    }

    subscribeDailyNewProperties(data) {
        return this.http.post(this.apiService.config + 'subscription/', data,
            this.options).pipe(map(response => response.json()));
    }

    getRateOfInterest() {
        return this.http.get(this.apiService.config + 'tax/RateOfInterest',
            this.options).pipe(map(response => response.json()));
    }

    preApproved(data) {
        return this.http.post(this.apiService.config + 'mortgage/', data,
            this.options).pipe(map(response => response.json()));
    }

    scheduleVisit(data) {
        return this.http.post(this.apiService.config + 'scheduleVisit/', data,
            this.options).pipe(map(response => response.json()));
    }
}