import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PropertyDetailService } from '../property-detail.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service';

@Component({
	selector: 'subscribe-newsletter',
	templateUrl: './subscribe-newsletter.page.html',
	styleUrls: ['./subscribe-newsletter.page.scss'],
})

export class SubscribeNewsletterPage implements OnInit {

	leadGenerationForm: FormGroup;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
	public basementArray = [];
	public propertyTypesArray = [];
	public minPriceList = [];
	public maxPriceList = [];
	public minSquareFootageList = [];
	public maxSquareFootageList = [];
	saleFlag: boolean = true;
	residentialFlag: boolean = true;
	saleOrLease: any = 'Sale';
	propertyType: any = 'residential';
	bedRooms: any[] = [];
	bathRoom: any = '';
	garageParking: any = '';
	bedroomAny: boolean = true;
	bedroom1: boolean = false;
	bedroom2: boolean = false;
	bedroom3: boolean = false;
	bedroom4: boolean = false;
	bedroom5: boolean = false;
	bathroomAny: boolean = true;
	bathroom1: boolean = false;
	bathroom2: boolean = false;
	bathroom3: boolean = false;
	bathroom4: boolean = false;
	bathroom5: boolean = false;
	gpAny: boolean = true;
	gp1: boolean = false;
	gp2: boolean = false;
	gp3: boolean = false;
	gp4: boolean = false;
	gp5: boolean = false;
	detailFormFlag: boolean = true;

	constructor(public router: Router, public loadingCtrl: LoadingController,
		public formBuilder: FormBuilder, public toastCtrl: ToastController,
		public leadGenerationService: PropertyDetailService, private data: DataService) {
	}

	ngOnInit() {
		this.leadGenerationFormDetails();
		this.getBasementTypes();
		this.getMinPrices();
		this.getMaxPrices();
		this.getMinSquareFootage();
		this.getMaxSquareFootage();
		this.getPropertyTypes('res');
	}

	leadGenerationFormDetails() {
		this.leadGenerationForm = this.formBuilder.group({
			name: '',
			phone: ['', [Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			address: '',
			propertyOptions: '',
			basementStyle: '',
			listPriceMin: null,
			listPriceMax: null,
			approxSquareFootageMin: null,
			approxSquareFootageMax: null,
			message: '',
			areaRadius: '1'
		});
	}

	showMore() {
		if (this.detailFormFlag) {
			this.detailFormFlag = false;
		} else {
			this.detailFormFlag = true;
		}
	}

	async subscribeToDailyNews(value) {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		await loading.present();
		var subscriptionData = {
			name: value.name,
			email: value.email,
			phone: value.phone,
			address: value.address,
			basementStyle: value.basementStyle,
			message: value.message,
			propertyType: this.propertyType,
			propertyOption: value.propertyOptions,
			saleLease: this.saleOrLease,
			bedRooms: this.bedRooms,
			washrooms: this.bathRoom,
			parkingSpaces: this.garageParking,
			areaRadius: value.areaRadius,
			areaMin: value.approxSquareFootageMin,
			areaMax: value.approxSquareFootageMax,
			priceMin: value.listPriceMin,
			priceMax: value.listPriceMax,
			isActive: true
		}
		this.leadGenerationService.subscribeDailyNewProperties(subscriptionData).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.presentToast('Subscription Added Successfully');
				this.router.navigateByUrl('/home-results');
			} else {
				this.presentToast('Something went wrong');
			}
		}, err => {
			loading.dismiss();
			this.presentToast('Something went wrong');
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	saleLease(value) {
		if (value == 'lease') {
			this.saleFlag = false;
			this.saleOrLease = 'Lease';
		} else {
			this.saleFlag = true;
			this.saleOrLease = 'Sale';
		}
	}

	propertyTypeChange(value) {
		if (value == 'commercial') {
			this.getPropertyTypes('commer');
			this.residentialFlag = false;
			this.propertyType = 'commercial';
			this.bedRooms = [];
			this.bathRoom = "";
			this.garageParking = "";
		} else {
			this.getPropertyTypes('res');
			this.residentialFlag = true;
			this.propertyType = 'residential';
		}
	}

	bedroom(value) {
		this.bedroomAny = false;
		if (value === 'any') {
			this.bedroom1 = false;
			this.bedroom2 = false;
			this.bedroom3 = false;
			this.bedroom4 = false;
			this.bedroom5 = false;
			this.bedroomAny = true;
			this.bedRooms = [];
		} else if (value === '1') {
			if (this.bedroom1) {
				if (this.bedRooms.includes("1")) {
					const index = this.bedRooms.indexOf('1');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("1");
			}
			if (this.bedroom5 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom1) {
					if (!this.bedroom2 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom1 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom1 = false;
					}
				} else {
					this.bedroom1 = true;
				}
			}
		} else if (value === '2') {
			if (this.bedroom2) {
				if (this.bedRooms.includes("2")) {
					const index = this.bedRooms.indexOf('2');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("2");
			}
			if (this.bedroom1 && this.bedroom5 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom2) {
					if (!this.bedroom1 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom2 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom2 = false;
					}
				} else {
					this.bedroom2 = true;
				}
			}
		} else if (value === '3') {
			if (this.bedroom3) {
				if (this.bedRooms.includes("3")) {
					const index = this.bedRooms.indexOf('3');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("3");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom5 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom3) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom3 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom3 = false;
					}
				} else {
					this.bedroom3 = true;
				}
			}
		} else if (value === '4') {
			if (this.bedroom4) {
				if (this.bedRooms.includes("4")) {
					const index = this.bedRooms.indexOf('4');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("4");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom5) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom4) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom5) {
						this.bedroom4 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom4 = false;
					}
				} else {
					this.bedroom4 = true;
				}
			}
		} else if (value === '5') {
			if (this.bedroom5) {
				if (this.bedRooms.includes("5+")) {
					const index = this.bedRooms.indexOf('5+');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("5+");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom5) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom4) {
						this.bedroom5 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom5 = false;
					}
				} else {
					this.bedroom5 = true;
				}
			}
		}
	}

	bathroom(value) {
		this.bathroom1 = false;
		this.bathroom2 = false;
		this.bathroom3 = false;
		this.bathroom4 = false;
		this.bathroom5 = false;
		this.bathroomAny = false;
		if (value === 'any') {
			this.bathroomAny = true;
			this.bathRoom = '';
		} else if (value === '1') {
			this.bathroom1 = true;
			this.bathRoom = '1';
		} else if (value === '2') {
			this.bathroom2 = true;
			this.bathRoom = '2';
		} else if (value === '3') {
			this.bathroom3 = true;
			this.bathRoom = '3';
		} else if (value === '4') {
			this.bathroom4 = true;
			this.bathRoom = '4';
		} else if (value === '5') {
			this.bathroom5 = true;
			this.bathRoom = '5';
		}
	}

	gp(value) {
		this.gp1 = false;
		this.gp2 = false;
		this.gp3 = false;
		this.gp4 = false;
		this.gp5 = false;
		this.gpAny = false;
		if (value === 'any') {
			this.gpAny = true;
			this.garageParking = '';
		} else if (value === '1') {
			this.gp1 = true;
			this.garageParking = '1';
		} else if (value === '2') {
			this.gp2 = true;
			this.garageParking = '2';
		} else if (value === '3') {
			this.gp3 = true;
			this.garageParking = '3';
		} else if (value === '4') {
			this.gp4 = true;
			this.garageParking = '4';
		} else if (value === '5') {
			this.gp5 = true;
			this.garageParking = '5';
		}
	}

	getPropertyTypes(type) {
		this.propertyTypesArray = [];
		if (type == 'res') {
			this.propertyTypesArray = [
				{ id: 1, name: 'Condo Apt' },
				{ id: 2, name: 'Condo Townhouse' },
				{ id: 3, name: 'Detached' },
				{ id: 4, name: 'Semi-Detached' },
				{ id: 5, name: 'Vacant Land' }
			]
		} else {
			this.propertyTypesArray = [
				{ id: 1, name: 'Business' },
				{ id: 2, name: 'Office' },
				{ id: 3, name: 'All' }
			]
		}
	}

	getBasementTypes() {
		this.basementArray = this.data.getBasements();
	}

	getMinPrices() {
		this.minPriceList = this.data.getMinPrices();
	}

	getMaxPrices() {
		this.maxPriceList = this.data.getMaxPrices();
	}

	getMinSquareFootage() {
		this.minSquareFootageList = this.data.getMinSquareFootage();
	}

	getMaxSquareFootage() {
		this.maxSquareFootageList = this.data.getMaxSquareFootage();
	}
}