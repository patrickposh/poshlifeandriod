import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MatExpansionModule, MatButtonModule, MatSelectModule, MatCheckboxModule, MatListModule, MatFormFieldModule, MatRadioModule, MatInputModule, MatIconModule } from '@angular/material';
import { IonicModule } from '@ionic/angular';
import { PropertyDetailPage } from './property-detail.page';
import { PropertyDetailService } from './property-detail.service';
import { StarRatingModule } from 'ionic4-star-rating';
import { ScheduleVisitBottom } from './property-detail.page';
import { SubscribeNewsletterPage } from './subscribe-newsletter/subscribe-newsletter.page';
import { PreApprovedMe } from './property-detail.page';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

const routes: Routes = [
	{
		path: '',
		component: PropertyDetailPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		MatExpansionModule,
		MatSelectModule,
		MatIconModule,
		MatCheckboxModule,
		MatListModule,
		MatFormFieldModule,
		MatRadioModule,
		MatInputModule,
		MatButtonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		StarRatingModule,
		MatBottomSheetModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild(),
	],
	exports: [
		SubscribeNewsletterPage
	],
	declarations: [
		PropertyDetailPage,
		SubscribeNewsletterPage,
		ScheduleVisitBottom,
		PreApprovedMe
	],
	providers: [
		PropertyDetailService
	],
	entryComponents: [
		ScheduleVisitBottom,
		PreApprovedMe
	]
})
export class PropertyDetailPageModule { }