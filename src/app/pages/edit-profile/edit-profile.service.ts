import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiConfigService } from '../../api.service';
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';

@Injectable()
export class EditProfileService {

    options: any;
    fileOptions: any;
    token: any;
    options1: { headers: HttpHeaders; };

    constructor(public http: Http, public apiService: ApiConfigService) {

        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        if (loginDetails != undefined) {
            this.token = loginDetails.data.token
        }

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new RequestOptions({ headers: headers });

        const fileHeaders = new Headers();
        // fileHeaders.append('Access-Control-Allow-Origin', '*');
        fileHeaders.append('Authorization', 'Bearer ' + this.token);
        this.fileOptions = new RequestOptions({ headers: fileHeaders });
    }

    getUserDetails(userId) {
        return this.http.get(this.apiService.config + 'user/' + userId,
            this.options).pipe(map(response => response.json()));
    }

    updateUserDetails(data) {
        return this.http.put(this.apiService.config + 'user/', data,
            this.options).pipe(map(response => response.json()));
    }

    uploadProfilePic(data) {
        return this.http.post(this.apiService.config + 'user/fileUploader', data,
            this.fileOptions).pipe(map(response => response.json()));
    }

    uploadUserPicturemobile(file){
        var imageData = new FormData();
        imageData.append("file",file.name[0]);
        return this.http.post(this.apiService.config + 'user/fileUploader',imageData,this.fileOptions).pipe(map(response => response.json()));;
      }
}