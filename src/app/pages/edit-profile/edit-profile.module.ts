import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { EditProfilePage } from './edit-profile.page';
import { EditProfileService } from './edit-profile.service';
import { MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { Camera } from '@ionic-native/camera/ngx';

const routes: Routes = [
	{
		path: '',
		component: EditProfilePage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		MatIconModule,
		MatFormFieldModule,
		MatInputModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		TranslateModule.forChild(),
		RouterModule.forChild(routes)
	],
	declarations: [
		EditProfilePage
	],
	providers: [
		EditProfileService,
		Camera
	]
})
export class EditProfilePageModule { }
