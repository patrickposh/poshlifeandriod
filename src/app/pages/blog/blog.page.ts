import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';
import { NavController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BlogInfoPage } from '../modal/blog-info/blog-info.page';
import { BlogService } from './blog.service';
import { ApiConfigService } from 'src/app/api.service';

@Component({
	selector: 'app-blog',
	templateUrl: './blog.page.html',
	styleUrls: ['./blog.page.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
				query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class BlogPage implements OnInit {
	blogs: any[] = [];
	imageUrl: any;
	loginRequired: boolean = false;

	constructor(public modalCtrl: ModalController, public navCtrl: NavController,
		public toastCtrl: ToastController, public loadingCtrl: LoadingController,
		private blogService: BlogService, public apiService: ApiConfigService,
		private translateService: TranslateService) {

		var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (loginDetails == undefined) {
			this.loginRequired = true;
		}

		this.imageUrl = this.apiService.config + 'blog/download-blogImage/';
	}

	ngOnInit() {
		this.getAllBlogs();
	}

	async getAllBlogs() {
		var loading: any;
		this.translateService.get('loading').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.blogService.getAllBlogs().subscribe(async res => {
			await loading.dismiss();
			this.blogs = res.data;
		}, err => {
			loading.dismiss();
			if (err.status == 401) {
				this.presentToast("Session Timeout");
				localStorage.removeItem('userDetails');
				localStorage.removeItem('modalShown');
				this.navCtrl.navigateRoot('/login');
				setTimeout(() => {
					window.location.reload();
				}, 300);
			}
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	async blogInfo(blog) {
		const modal = await this.modalCtrl.create({
			component: BlogInfoPage,
			componentProps: {
				blogDetail: blog
			}
		});
		return await modal.present();
	}
}