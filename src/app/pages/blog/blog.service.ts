import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiConfigService } from '../../api.service';
import { map } from "rxjs/operators";

@Injectable()
export class BlogService {

    options: any;
    token: any;

    constructor(public http: Http, public apiService: ApiConfigService) {

        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        if (loginDetails != undefined) {
            this.token = loginDetails.data.token
        }

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new RequestOptions({ headers: headers });
    }

    getAllBlogs() {
        return this.http.get(this.apiService.config + 'blog/',
            this.options).pipe(map(response => response.json()));
    }
}