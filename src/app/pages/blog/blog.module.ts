import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { MatExpansionModule, MatIconModule } from '@angular/material';
import { BlogPage } from './blog.page';
import { BlogService } from './blog.service';

const routes: Routes = [
	{
		path: '',
		component: BlogPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		MatExpansionModule,
		MatIconModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		TranslateModule.forChild(),
		RouterModule.forChild(routes)
	],
	declarations: [
		BlogPage
	],
	providers: [
		BlogService
	]
})
export class BlogPageModule { }