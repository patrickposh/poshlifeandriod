import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { RegisterPage } from './register.page';
import { RegisterService } from './register.service';
import { Facebook } from '@ionic-native/facebook/ngx';

const routes: Routes = [
	{
		path: '',
		component: RegisterPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		MatIconModule,
		MatFormFieldModule,
		MatInputModule,
		IonicModule,
		TranslateModule.forChild(),
		RouterModule.forChild(routes)
	],
	declarations: [
		RegisterPage
	],
	providers: [
		RegisterService,
		Facebook
	]
})

export class RegisterPageModule { }
