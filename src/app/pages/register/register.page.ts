import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn, ValidationErrors, AbstractControl } from '@angular/forms';
import { NavController, MenuController, LoadingController, ToastController } from '@ionic/angular';
import { RegisterService } from './register.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
	selector: 'app-register',
	templateUrl: './register.page.html',
	styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {
	registerForm: FormGroup;
	private _unsubscribeAll: Subject<any>;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

	constructor(public navCtrl: NavController, public menuCtrl: MenuController,
		public loadingCtrl: LoadingController, public toastCtrl: ToastController,
		private registerService: RegisterService, private formBuilder: FormBuilder,
		private facebook: Facebook) {

		this._unsubscribeAll = new Subject();
	}

	ionViewWillEnter() {
		this.menuCtrl.enable(false);
		this.menuCtrl.swipeEnable(false);
	}

	ngOnInit() {
		this.registerFormDetails();
	}

	registerFormDetails() {
		this.registerForm = this.formBuilder.group({
			firstname: '',
			lastname: '',
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			password: ['', Validators.required],
			confirmPassword: ['', [Validators.required, confirmPasswordValidator]]
		});

		this.registerForm.get('password').valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
			this.registerForm.get('confirmPassword').updateValueAndValidity();
		});
	}

	async register(value) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();
		this.registerService.register(value).subscribe(async data => {
			await loading.dismiss();
			if (data.code == 200) {
				this.presentToast(data.message);
				this.navCtrl.navigateRoot('/login');
			} else {
				this.presentToast(data.message);
			}
		}, err => {
			loading.dismiss();
			this.presentToast(err);
		})
	}

	signUpWithFacebok() {
		var userData: any = null;;
		this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
			this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
				userData = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'] }
				console.log("d",userData);
			});
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	login() {
		this.navCtrl.navigateRoot('/login');
	}
}

export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

	if (!control.parent || !control) {
		return null;
	}

	const password = control.parent.get('password');
	const confirmPassword = control.parent.get('confirmPassword');

	if (!password || !confirmPassword) {
		return null;
	}

	if (confirmPassword.value === '') {
		return null;
	}

	if (password.value === confirmPassword.value) {
		return null;
	}

	return { passwordsNotMatching: true };
};
