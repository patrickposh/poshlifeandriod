import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiConfigService } from '../../api.service';
import { map } from "rxjs/operators";

@Injectable()
export class HomeResultsService {

    options: any;

    constructor(public http: Http, public apiService: ApiConfigService) {

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new RequestOptions({ headers: headers });
    }

    filterProperties(data) {
        return this.http.post(this.apiService.config + 'property/searchAttributes', data,
            this.options).pipe(map(response => response.json()));
    }

    searchProperties(data) {
        return this.http.post(this.apiService.config + 'property/search', data,
            this.options).pipe(map(response => response.json()));
    }
}