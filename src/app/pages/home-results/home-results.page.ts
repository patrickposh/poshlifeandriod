import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, LoadingController, ModalController, ActionSheetController, Platform, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HomeResultsService } from './home-results.service';

@Component({
	selector: 'app-home-results',
	templateUrl: './home-results.page.html',
	styleUrls: ['./home-results.page.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
				query(':enter', stagger('200ms', [animate('400ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class HomeResultsPage implements OnInit {
	properties: any[] = [];
	currentLat: any = "";
	currentLng: any = "";
	userId: any = "";
	loginFlag: boolean = true;
	profileView: boolean = true;
	saleLease: any = 'Sale';
	propertyType: any = 'residential';
	cnt = 1;
	public searchForm: FormGroup;
	public searchValues: any[] = [];
	isLoading: boolean;
	moreResults: boolean = true;
	public searchObject: any;
	public filterObject: any;
	loginDetails: any;
	searchText: any;
	isArabic: boolean = false;

	constructor(public navCtrl: NavController, public menuCtrl: MenuController,
		public modalCtrl: ModalController, public loadingCtrl: LoadingController,
		public toastCtrl: ToastController, public translate: TranslateService,
		public homeResultService: HomeResultsService, public fb: FormBuilder,
		private geolocation: Geolocation, private asCtrl: ActionSheetController,
		private platform: Platform) {

		this.loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		var language = localStorage.getItem('language');
		if (language != null && language != undefined) {
			if (language == 'ar' || language == 'per') {
				this.isArabic = !this.isArabic;
			}
		}
		if (this.loginDetails != undefined) {
			this.loginFlag = false;
			this.profileView = false;
			this.userId = JSON.stringify(this.loginDetails.data.userVM.id);
		}
		this.translate.setDefaultLang(environment.language);
	}

	public advancedFilterObject() {
		return {
			address: "",
			approxSquareFootageMax: null,
			approxSquareFootageMin: null,
			basement1: "",
			bedRooms: [],
			currentPage: this.cnt,
			daysOnMarket: "",
			latitude: this.currentLat,
			longitude: this.currentLng,
			listPriceMax: null,
			listPriceMin: null,
			openHouse: "-1",
			parkingSpaces: "",
			propertyOption: "",
			propertyType: this.propertyType,
			soldDays: "",
			saleLease: this.saleLease,
			userId: this.userId,
			washrooms: "",
			withSold: false
		}
	}

	ngOnInit() {
		this.searchForm = this.fb.group({
			myControl: new FormControl()
		});
	}

	searchProperty() {
		var key = this.searchForm.get("myControl").value;
		if (key && key.length > 2) {
			this.isLoading = true;
			var searchData = {
				currentPage : 1,
				searchText : key
			}
			this.homeResultService.searchProperties(searchData).subscribe(data => {
				this.isLoading = false;
				if (data.data != undefined) {
					if (data.data && data.data.length > 0) {
						var add = { locationSearch: data.data[0].locationSearch, data: data.data[0] };
						this.searchValues = [
							{
								name: 'Locations',
								data: [add]
							},
							{
								name: 'Listing',
								data: data.data
							}
						]
					} else {
						this.searchValues = [];
					}
				}
			}, err => {
				this.isLoading = false;
			});
		} else {
			this.searchValues = [];
		}
	}

	ionViewWillEnter() {
		this.cnt = 1;
		this.filterObject = this.advancedFilterObject();
		this.geolocation.getCurrentPosition().then((resp) => {
			this.currentLat = (resp.coords.latitude).toString();
			this.currentLng = (resp.coords.longitude).toString();
			this.filterObject.latitude = this.currentLat;
			this.filterObject.longitude = this.currentLng;
			this.findAll();
		}, err => {
			this.currentLat = "43.63943345";
			this.currentLng = "-79.39972759226308";
			this.filterObject.latitude = this.currentLat;
			this.filterObject.longitude = this.currentLng;
			this.findAll();
		});
		this.menuCtrl.enable(true);
		this.cnt = 1;
		this.moreResults = true;
	}

	ionViewDidEnter() {
		// this.platform.backButton.subscribeWithPriority(2, () => {
		// 	navigator['app'].exitApp();
		// });
	}

	ionViewDidLeave(): void {
		this.menuCtrl.enable(true);
	}

	settings() {
		this.navCtrl.navigateForward('settings');
	}

	async findAll() {
		var loading: any;
		this.translate.get('app.pleaseWait').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.homeResultService.filterProperties(this.filterObject).subscribe(async data => {
			await loading.dismiss();
			this.properties = data.data;

			/* ****************CONDITION TO CHECK LOG IN USER ***************/
			if (this.saleLease === 'Lease') {
				this.loginFlag = false;
			} else {
				if (this.loginDetails != undefined && this.loginDetails != null) {
					this.loginFlag = false;
				} else {
					this.loginFlag = true;
				}
			}
		}, err => {
			loading.dismiss();
			this.translate.get('property.failedToLoadProperties').subscribe(async value => {
				this.presentToast(value);
			});
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	saleLeaseChange(value) {
		this.cnt = 0;
		if (value) {
			this.saleLease = 'Lease';
			this.filterObject.saleLease = 'Lease';
			this.filterObject.soldDays = "";
		} else {
			this.saleLease = 'Sale';
			this.filterObject.saleLease = 'Sale';
		}
		this.findAll();
	}

	propertyTypeChange(value) {
		this.cnt = 0;
		if (value) {
			this.propertyType = 'commercial';
			this.filterObject.propertyType = 'commercial';
		} else {
			this.propertyType = 'residential';
			this.filterObject.propertyType = 'residential';
		}
		this.findAll();
	}

	async searchFilter() {
		const modal = await this.modalCtrl.create({
			component: SearchFilterPage,
			componentProps: {
				setFilterObject: this.filterObject
			}
		});

		modal.onDidDismiss().then((dataReturned) => {
			if (dataReturned !== null) {
				if (dataReturned.data != null) {
					this.filterObject = dataReturned.data;
					if (dataReturned.data.saleLease === 'Lease') {
						this.saleLease = 'Lease';
						this.loginFlag = false;
					} else if (dataReturned.data.saleLease === 'Sale') {
						this.saleLease = 'Sale';
						if (this.loginDetails != undefined && this.loginDetails != null) {
							this.loginFlag = false;
						} else {
							this.loginFlag = true;
						}
					}
					this.findAll();
				}
			}
		});
		return await modal.present();
	}

	async language() {
		this.translate.get(['app.selectLanguage', 'app.cancel']).subscribe(async value => {
			const actionSheet = await this.asCtrl.create({
				header: value['app.selectLanguage'],
				buttons: [{
					text: 'English',
					handler: () => {
						this.translate.use('en');
						localStorage.setItem("language", 'en');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: 'Española',
					handler: () => {
						this.translate.use('sp');
						localStorage.setItem("language", 'sp');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: 'हिंदी',
					handler: () => {
						this.translate.use('hn');
						localStorage.setItem("language", 'hn');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: 'Française',
					handler: () => {
						this.translate.use('fr');
						localStorage.setItem("language", 'fr');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: 'عربى',
					handler: () => {
						this.translate.use('ar');
						localStorage.setItem("language", 'ar');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: '中文',
					handler: () => {
						this.translate.use('ch');
						localStorage.setItem("language", 'ch');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: 'فارسی',
					handler: () => {
						this.translate.use('per');
						localStorage.setItem("language", 'per');
						setTimeout(() => {
							window.location.reload();
						}, 150);
					}
				}, {
					text: value['app.cancel'],
					role: 'cancel',
				}]
			});
			await actionSheet.present();
		})
	}

	async moreProperties() {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		await loading.present();
		this.cnt = this.cnt + 1;
		this.filterObject.currentPage = this.cnt;
		this.homeResultService.filterProperties(this.filterObject).subscribe(async res => {
			await loading.dismiss();
			if (res.data.length === 0) {
				this.moreResults = false;
			}
			res.data.forEach(element => {
				this.properties.push(element);
			});
		}, err => {
			loading.dismiss();
			console.log(err);
		});
	}

	redirectToMap(data) {
		if (data === 'address') {
			this.cnt = -1;
			this.saleLease = "Sale";
			this.propertyType = "residential";
			this.filterObject = this.advancedFilterObject();
		} else {
			this.filterObject.saleLease = data.saleLease;
			this.filterObject.propertyType = data.propertyType;
			this.filterObject.latitude = data.latitude;
			this.filterObject.longitude = data.longitude;
		}
		setTimeout(() => {
			var filter = JSON.stringify(this.filterObject);
			this.navCtrl.navigateForward('/map-view', { state: { filterData: filter } });
		}, 150);
	}

	goToMap(type) {
		var filterObj = this.filterObject;
		if (type === 'commercialSale') {
			filterObj.propertyType = 'commercial';
			filterObj.saleLease = 'Sale';
		} else if (type === 'commercialLease') {
			filterObj.propertyType = 'commercial';
			filterObj.saleLease = 'Lease';
		} else if (type === 'residentialSale') {
			filterObj.propertyType = 'residential';
			filterObj.saleLease = 'Sale';
		} else if (type === 'residentialLease') {
			filterObj.propertyType = 'residential';
			filterObj.saleLease = 'Lease';
		}

		setTimeout(() => {
			var filter = JSON.stringify(filterObj);
			this.navCtrl.navigateForward('/map-view', { state: { filterData: filter } });
		}, 150);
	}
}
