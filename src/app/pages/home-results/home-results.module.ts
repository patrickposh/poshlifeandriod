import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicModule } from '@ionic/angular';
import { MatAutocompleteModule, MatInputModule, MatFormFieldModule, MatOptionModule, MatIconModule } from '@angular/material';
import { PipesModule } from '../../pipes/pipes.module';
import { HomeResultsPage } from './home-results.page';
import { HomeResultsService } from './home-results.service';

export function HttpLoaderFactory(httpClient: HttpClient) {
	return new TranslateHttpLoader(httpClient);
}


const routes: Routes = [
	{
		path: '',
		component: HomeResultsPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		PipesModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient]
			}
		}),
		MatAutocompleteModule,
		MatFormFieldModule,
		MatOptionModule,
		MatInputModule,
		MatIconModule
	],
	declarations: [
		HomeResultsPage
	],
	providers: [
		HomeResultsService
	]
})
export class HomeResultsPageModule { }
