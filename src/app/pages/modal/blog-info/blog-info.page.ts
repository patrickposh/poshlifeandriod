import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiConfigService } from 'src/app/api.service';

@Component({
    selector: 'app-blog-info',
    templateUrl: './blog-info.page.html',
    styleUrls: ['./blog-info.page.scss'],
})
export class BlogInfoPage implements OnInit {
    @Input() blogDetail: any;
    imageUrl: any;
    blog: any;

    constructor(private modalCtrl: ModalController, public apiService: ApiConfigService) {
        this.imageUrl = this.apiService.config + 'blog/download-blogImage/';
    }

    ngOnInit() {
        this.blog = this.blogDetail;
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }
}