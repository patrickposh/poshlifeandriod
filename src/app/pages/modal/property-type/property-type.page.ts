import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-property-type',
    templateUrl: './property-type.page.html',
    styleUrls: ['./property-type.page.scss'],
})
export class PropertyTypePage implements OnInit {
    @Input() data: any;
    property: any;

    constructor(private modalCtrl: ModalController) { }

    ngOnInit() {
        // this.property = this.data;
    }

    closeModal() {
        this.modalCtrl.dismiss();
    }
}