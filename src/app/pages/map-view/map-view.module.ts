import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { MatListModule, MatAutocompleteModule, MatInputModule, MatFormFieldModule, MatOptionModule, MatIconModule } from '@angular/material';
import { PipesModule } from '../../pipes/pipes.module';
import { PropertyInfoSheet } from './map-view.page';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MapViewPage } from './map-view.page';
import { MapViewService } from './map-view.service';

const routes: Routes = [
	{
		path: '',
		component: MapViewPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		MatListModule,
		PipesModule,
		MatAutocompleteModule,
		MatFormFieldModule,
		MatOptionModule,
		MatInputModule,
		MatIconModule,
		MatBottomSheetModule,
		RouterModule.forChild(routes),
		TranslateModule.forChild()
	],
	declarations: [
		MapViewPage,
		PropertyInfoSheet
	],
	providers: [
		MapViewService
	],
	entryComponents: [
		PropertyInfoSheet
	]
})
export class MapViewPageModule { }
