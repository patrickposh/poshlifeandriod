import { Component, Inject, OnInit } from '@angular/core';
import { NavController, ModalController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { SearchFilterPage } from '../modal/search-filter/search-filter.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ApiConfigService } from 'src/app/api.service';
import { MapViewService } from './map-view.service';
import * as moment from 'moment';
import * as L from "leaflet";
import 'leaflet.markercluster';

@Component({
	selector: 'app-map-view',
	templateUrl: './map-view.page.html',
	styleUrls: ['./map-view.page.scss']
})

export class MapViewPage implements OnInit {
	properties: any[] = [];
	schools: any[] = [];
	currentLat: any = "";
	currentLng: any = "";
	map: L.Map;
	marker: any;
	markerCluster = L.markerClusterGroup({
		spiderfyOnMaxZoom: false
	});
	public searchForm: FormGroup;
	public searchValues: any[] = [];
	isLoading: boolean;
	public searchObject: any;
	public filterObject: any;
	propertyMarker: any = L.icon({
		iconUrl: '../../assets/icon/marker-icon.png',
		iconSize: [23, 40],
		iconAnchor: [9, 21],
		popupAnchor: [0, -14]
	});
	schoolMarker: any = L.icon({
		iconUrl: '../../assets/icon/school (1).png',
		iconSize: [25, 30],
		iconAnchor: [9, 21],
		popupAnchor: [0, -14]
	})
	saleFlag: boolean = true;
	propertyTypeFlag: boolean = true;
	showSchoolPopup: boolean = false;
	showSchoolFlag: boolean = false;
	satellite: boolean = false;
	currentZoomLevel = 15;
	radius = "1";
	saleLease = 'Sale';
	propertyType = 'residential';
	prevZoom: any;
	searchText: any;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController,
		public mapViewService: MapViewService, public toastCtrl: ToastController,
		public route: ActivatedRoute, private bottomSheet: MatBottomSheet,
		public router: Router, public fb: FormBuilder,
		public loadingCtrl: LoadingController, private geolocation: Geolocation,
		private translateService: TranslateService) { }

	public advancedFilterObject() {
		return {
			address: "",
			approxSquareFootageMax: null,
			approxSquareFootageMin: null,
			basement1: "",
			bedRooms: [],
			currentPage: -1,
			daysOnMarket: "",
			listPriceMax: null,
			listPriceMin: null,
			openHouse: "-1",
			parkingSpaces: "",
			propertyOption: "",
			soldDays: "",
			radius: this.radius,
			saleLease: this.saleLease,
			propertyType: this.propertyType,
			washrooms: "",
			latitude: this.currentLat,
			longitude: this.currentLng,
			withSold: false
		}
	}

	ngOnInit() {
		if (window.history.state.filterData) {
			this.filterObject = JSON.parse(window.history.state.filterData);
			this.currentLat = this.filterObject.latitude;
			this.currentLng = this.filterObject.longitude;
			this.filterObject.radius = this.radius;
			this.saleLease = this.filterObject.saleLease;
			this.propertyType = this.filterObject.propertyType;
			if (this.propertyType == 'commercial') {
				this.propertyTypeFlag = false;
			}
			if (this.saleLease == 'Lease') {
				this.saleFlag = false;
			}
			this.findAll();
		} else {
			this.geolocation.getCurrentPosition().then((resp) => {
				this.currentLat = (resp.coords.latitude).toString();
				this.currentLng = (resp.coords.longitude).toString();
				this.filterObject.latitude = this.currentLat;
				this.filterObject.longitude = this.currentLng;
				setTimeout(() => {
					this.findAll();
				}, 500);
			}, err => {
				this.currentLat = "43.63943345";
				this.currentLng = "-79.39972759226308";
				this.filterObject.latitude = this.currentLat;
				this.filterObject.longitude = this.currentLng;
				setTimeout(() => {
					this.findAll();
				}, 500);
			});
		}
		this.filterObject = this.advancedFilterObject();

		this.searchForm = this.fb.group({
			myControl: new FormControl()
		});

		this.map = L.map('myMap', {
			zoomControl: true
		});

		this.map.zoomControl.setPosition('bottomright');

		L.DomUtil.setOpacity(this.map.zoomControl.getContainer(), 0.8);

		this.prevZoom = this.map.getZoom();

		this.map.on('dragend', e => {
			var self = this;
			var latLng = e.target.getCenter();
			self.currentLat = (latLng.lat).toString();
			self.currentLng = (latLng.lng).toString();
			this.filterObject.latitude = this.currentLat;
			this.filterObject.longitude = this.currentLng;
			this.findAll();
		});

		this.map.on('zoomend', e => {
			var self = this;
			var latLng = e.target.getCenter();
			self.currentLat = (latLng.lat).toString();
			self.currentLng = (latLng.lng).toString();
			this.filterObject.latitude = this.currentLat;
			this.filterObject.longitude = this.currentLng;
			self.moreProperties();
		});

		this.map.setMinZoom(2);
	}

	searchProperty() {
		var key = this.searchForm.get("myControl").value;
		if (key && key.length > 2) {
			this.isLoading = true;
			var searchData = {
				currentPage: 1,
				searchText: key
			}
			this.mapViewService.searchProperties(searchData).subscribe(data => {
				this.isLoading = false;
				if (data.data != undefined) {
					if (data.data && data.data.length > 0) {
						var add = { locationSearch: data.data[0].locationSearch, data: data.data[0] };
						this.searchValues = [
							{
								name: 'Locations',
								data: [add]
							},
							{
								name: 'Listing',
								data: data.data
							}
						]
					} else {
						this.searchValues = [];
					}
				}
			}, err => {
				this.isLoading = false;
			});
		} else {
			this.searchValues = [];
		}
	}

	clearAddress() {
		this.filterObject = this.advancedFilterObject();
		this.findAll();
	}

	async findAll() {
		this.properties = [];
		var loading: any;
		this.translateService.get('loading').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.mapViewService.filterProperties(this.filterObject).subscribe(async data => {
			await loading.dismiss();
			data.data.forEach(element => {
				if (element.soldPrice === 0) {
					var listing = moment(element.listingEntryDate).format("DD/MM/YYYY");
					var startDate = moment(listing, "DD/MM/YYYY");
					var currenDate = moment(new Date()).format("DD/MM/YYYY");
					var endDate = moment(currenDate, "DD/MM/YYYY");
					element['difference_In_ListingDays'] = endDate.diff(startDate, 'days');
				} else {
					var listing = moment(element.soldDate).format("DD/MM/YYYY");
					var startDate = moment(listing, "DD/MM/YYYY");
					var currenDate = moment(new Date()).format("DD/MM/YYYY");
					var endDate = moment(currenDate, "DD/MM/YYYY");
					element['difference_In_SoldDays'] = endDate.diff(startDate, 'days');
				}
			});
			this.properties = data.data;
			if (data.data.length === 0) {
				this.presentToast(data.message);
			}
			this.markerCluster.clearLayers();
			this.map.setView([this.currentLat, this.currentLng], this.currentZoomLevel);
			this.showMap();
		}, err => {
			loading.dismiss();
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	showMap() {
		if (this.satellite) {
			L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}?', {
				maxZoom: 18,
				subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
			}).addTo(this.map);
		} else {
			L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
				maxZoom: 18,
				id: 'mapbox/streets-v11',
				tileSize: 512,
				zoomOffset: -1,
				accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
			}).addTo(this.map);
		}

		this.addClusters();
	}

	addClusters() {
		for (var i = 0; i < this.properties.length; ++i) {
			var m: any = L.marker([this.properties[i].latitude, this.properties[i].longitude], { icon: this.propertyMarker }).on('click', this.openBottomSheet.bind(this, [this.properties[i]]));
			m.myData = { property: this.properties[i] };
			this.markerCluster.addLayer(m);
		}

		this.markerCluster.on('clusterclick', a => {
			if (a.layer._zoom == 18) {
				var data: any;
				var tempArray = [];
				var markers = a.layer._markers;
				for (var i = 0; i < markers.length; i++) {
					data = markers[i].myData.property;
					tempArray.push(data);
				}
				this.openBottomSheet(tempArray, 'e');
			}
		});

		this.map.addLayer(this.markerCluster);
	}

	selectLocation(value) {
		this.currentLat = value.latitude;
		this.currentLng = value.longitude;
		this.filterObject.latitude = this.currentLat;
		this.filterObject.longitude = this.currentLng;
		this.getSearchAddressProperties();
	}

	getSearchAddressProperties() {
		this.properties = [];
		var loading: any;
		this.translateService.get('loading').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value
			});
			await loading.present();
		});
		this.mapViewService.filterProperties(this.filterObject).subscribe(async data => {
			await loading.dismiss();
			data.data.forEach(element => {
				if (element.soldPrice === 0) {
					var listing = moment(element.listingEntryDate).format("DD/MM/YYYY");
					var startDate = moment(listing, "DD/MM/YYYY");
					var currenDate = moment(new Date()).format("DD/MM/YYYY");
					var endDate = moment(currenDate, "DD/MM/YYYY");
					element['difference_In_ListingDays'] = endDate.diff(startDate, 'days');
				} else {
					var listing = moment(element.soldDate).format("DD/MM/YYYY");
					var startDate = moment(listing, "DD/MM/YYYY");
					var currenDate = moment(new Date()).format("DD/MM/YYYY");
					var endDate = moment(currenDate, "DD/MM/YYYY");
					element['difference_In_SoldDays'] = endDate.diff(startDate, 'days');
				}
			});
			this.properties = data.data;
			this.markerCluster.clearLayers();
			if (this.properties.length > 0) {
				this.map.setView([this.properties[0].latitude, this.properties[0].longitude], this.currentZoomLevel);
				this.currentLat = this.properties[0].latitude;
				this.currentLng = this.properties[0].longitude;
				this.filterObject.latitude = this.currentLat;
				this.filterObject.longitude = this.currentLng;
			} else {
				this.map.setView([this.currentLat, this.currentLng], this.currentZoomLevel);
			}
			this.showMap();
		}, err => {
			loading.dismiss();
		});
	}

	satelliteView() {
		this.satellite = !this.satellite;
		this.findAll();
	}

	moreProperties() {
		var currZoom = this.map.getZoom();
		this.currentZoomLevel = this.map.getZoom();
		switch (this.currentZoomLevel) {
			case 15:
				this.radius = "1";
				break;
			case 14:
				this.radius = "1";
				break;
			case 13:
				this.radius = "3";
				break;
			case 12:
				this.radius = "5";
				break;
			case 11:
				this.radius = "5";
				break;
			case 10:
				this.radius = "8";
				break;
			case 9:
				this.radius = "8";
				break;
			case 8:
				this.radius = "10";
				break;
			case 7:
				this.radius = "10";
				break;
			case 6:
				this.radius = "20";
				break;
			case 5:
				this.radius = "30";
				break;
			case 4:
				this.radius = "40";
				break;
			case 3:
				this.radius = "50";
				break;
			case 2:
				this.radius = "60";
				break;
			default:
				this.radius = "1";
				break;
		}
		this.filterObject.radius = this.radius;
		if (this.showSchoolFlag) {
			if (currZoom > 13) {
				this.showSchoolsOnMap();
			} else {
				this.map.eachLayer((layer) => {
					layer.remove();
				});
				this.findAll();
			}
		} else {
			this.findAll();
		}
	}

	saleLeaseChange(value) {
		if (value == 'Lease') {
			this.saleFlag = false;
			this.saleLease = 'Lease';
			this.filterObject.saleLease = 'Lease';
		} else {
			this.saleFlag = true;
			this.saleLease = 'Sale';
			this.filterObject.saleLease = 'Sale';
		}
		this.findAll();
	}

	propertyTypeChange(value) {
		if (value == 'commercial') {
			this.propertyTypeFlag = false;
			this.propertyType = 'commercial';
			this.filterObject.propertyType = 'commercial';
		} else {
			this.propertyTypeFlag = true;
			this.propertyType = 'residential';
			this.filterObject.propertyType = 'residential';
		}
		this.findAll();
	}

	async searchFilter() {
		const modal = await this.modalCtrl.create({
			component: SearchFilterPage,
			componentProps: {
				setFilterObject: this.filterObject
			}
		});
		modal.onDidDismiss().then((dataReturned) => {
			if (dataReturned !== null) {
				if (dataReturned.data != null) {
					this.filterObject = dataReturned.data;
					if (dataReturned.data.saleLease === 'Sale') {
						this.saleFlag = true;
					} else {
						this.saleFlag = false;
					}
					if (dataReturned.data.propertyType === 'residential') {
						this.propertyTypeFlag = true;
					} else {
						this.propertyTypeFlag = false;
					}
					this.filterObject.currentPage = -1;
					this.filterObject.radius = this.radius;
					this.findAll();
				}
			}
		});
		return await modal.present();
	}

	openBottomSheet(e, data): void {
		this.bottomSheet.open(PropertyInfoSheet, { data: e });
	}

	schoolPopup() {
		this.showSchoolPopup = !this.showSchoolPopup;
	}

	async showSchool(value) {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		if (value) {
			let data = {
				latitude: this.currentLat,
				longitude: this.currentLng,
				radious: 1500
			}
			this.mapViewService.showSchools(data).subscribe(async res => {
				await loading.dismiss();
				if (res.code == 200) {
					this.schools = res.data.results;
					this.showSchoolsOnMap();
				}
			}, err => {
				loading.dismiss();
			});
		} else {
			loading.dismiss();
			this.map.eachLayer((layer) => {
				layer.remove();
			});
			this.findAll();
		}
	}

	showSchoolsOnMap() {
		var marker: any;
		if (this.schools.length > 0) {
			for (var i = 0; i < this.schools.length; ++i) {
				marker = L.marker([this.schools[i].geometry.location.lat, this.schools[i].geometry.location.lng], { icon: this.schoolMarker });
				marker.bindPopup("<div class='row popup-width'>" +
					"<b style='font-size:14px'>" + this.schools[i].name + "</b><br>" +
					"<div style='margin-top:5px'>" + this.schools[i].formatted_address + "</div>" +
					"</div>"
				).addTo(this.map);
			}
		}
	}

	goToDetailPage(id) {
		setTimeout(() => {
			this.router.navigate(['property-detail/' + id]);
		}, 500);
	}
}

@Component({
	selector: 'property-info-sheet',
	styleUrls: ['./property-info-sheet.scss'],
	templateUrl: './property-info-sheet.html',
})

export class PropertyInfoSheet {
	properties = [];
	imageUrl: any;
	loginFlag: boolean = true;

	constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any, public router: Router,
		public platform: Platform, public apiService: ApiConfigService,
		private _bottomSheetRef: MatBottomSheetRef<PropertyInfoSheet>) {

		this.properties = data;
		this._bottomSheetRef.disableClose = false;

		var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (loginDetails != undefined) {
			this.loginFlag = false;
		}

		this.properties.forEach(element => {
			if (element.saleLease == 'Lease') {
				this.loginFlag = false;
			}
		});


		this.platform.backButton.subscribeWithPriority(10, () => {
			this._bottomSheetRef.dismiss();
		});

		this.imageUrl = this.apiService.config + 'property/map-image/';
	}

	openLink(event: MouseEvent, id): void {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
		this.router.navigateByUrl('/property-detail/' + id);
	}

	closePopup(event: MouseEvent): void {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
	}

	login() {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
		this.router.navigateByUrl('/login');
	}
}